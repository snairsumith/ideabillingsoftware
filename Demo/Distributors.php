<!DOCTYPE html>
<html>
    <head>
        
        <meta charset="utf-8" />
        <title>Billings </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="assets/plugins/morris/morris.css">
        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <!-- MetisMenu CSS -->
        <link href="assets/css/metisMenu.min.css" rel="stylesheet">
        <!-- Icons CSS -->
        <link href="assets/css/icons.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="assets/css/style.css" rel="stylesheet">
        
    </head>
    <body>
        <div id="page-wrapper">
            
            <?php include "includes/header.php"; ?>
            <!-- Page content start -->
            <div class="page-contentbar">
                <?php include "includes/side-menu.php"; ?>
                <!-- START PAGE CONTENT -->
                <div id="page-right-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="col-md-12 m-b-20">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <h4 class="m-b-20"><b>Distributors Details</b></h4>
                                        </div>
                                        <div class="col-md-3">
                                            <button type="button" id="btnNew" class="btn btn-primary pull-right">Add New</button>
                                            
                                            
                                        </div>
                                        
                                    </div>
                                    
                                    
                                    <div class="row">
                                        <table class="table table-bordered m-0">
                                            <thead>
                                                <tr>
                                                    <th>Distributor Code</th>
                                                    <th>Name</th>
                                                   
                                                    <th>Mobile No</th>
                                                    <th>GST No</th>
                                                    <th>Address</th>
                                                    <th>Location</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                <td scope="row">DIS100</th>
                                                <td>NEW WORLD COMMUNICATION CHAVADI ALP</td>
                                               
                                                <td>7511121507</td>
                                                <td>32AAFCS6843P1Z</td>
                                                
                                                <td>NEW WORLD COMMUNICATION, PARAYAKAD POST,CHERTHALA, ALAPPUZHA DISTRICT, Kerala, 688540</td>
                                                <td>ALP</td>
                                                <td>
                                                    <a href="#" onclick="editPopUp();">Edit</a> /
                                                    <a href="#" onclick="deletePopUp();">Delete</a>
                                                </td>
                                            </tr>
                                            <tr>
                                            <td scope="row">DIS101</th>
                                            <td>JADEED AGENCIES AYANCHERI CAL</td>
                                            <td>+919656761101</td>
                                            <td>32AAFCS6843P1Z</td>
                                            <td>JADEED AGENCIES AYANCHERI CAL, Thirvallur Road, Ayancheri, Kozhikode, Kerala, 673542</td>
                                            <td>CTL</td>
                                            <td>
                                                <a href="#" onclick="editPopUp();">Edit</a> /
                                                <a href="#" onclick="deletePopUp();">Delete</a>
                                            </td>
                                        </tr>
                                        <tr>
                                        <td scope="row">DIS102</th>
                                        <td>SUDHARSHANA COMMUNICATION MUTHAMBI CAL</td>
                                        <td>+919656761101</td>
                                        <td>32AAFCS6843P1Z</td>
                                        <td>SUDHARSHANA COMMUNICATION MUTHAMBI CAL , SUDHARSHANA COMMUNICATION, SUBASH NIVAS CHELOT THAZHE, PANTHALAYANI,KOYILANDY
</td>
                                        <td>CTL</td>
                                        <td>
                                            <a href="#" onclick="editPopUp();">Edit</a> /
                                            <a href="#" onclick="deletePopUp();">Delete</a>
                                        </td>
                                    </tr>
                                    <tr>
                                    <td scope="row">DIS103</th>
                                    <td>FR TRADERS THOTTUMUGHOM ALUVA EKM
</td>
                                    
                                    <td>+919656761101</td>
                                    <td>32AAFCS6843P1Z</td>
                                    <td>FR TRADERS, 2,550, PALLIKUZHIYIL HOUSE,THOTTUMUGHAM, ALUVA,Ernakulam, Kerala, 683105,
</td>
                                    <td>EKM</td>
                                    <td>
                                        <a href="#" onclick="editPopUp();">Edit</a> /
                                        <a href="#" onclick="deletePopUp();">Delete</a>
                                    </td>
                                </tr>
                                
        </tbody>
    </table>
</div>

</div>
</div>
</div>
<!--end row -->
<div class="row"></div> <!-- end row -->

</div>
<!-- end container -->
<?php include "includes/footer.php"; ?>
</div>
<!-- End #page-right-content -->
</div>
<!-- end .page-contentbar -->
</div>
<!-- End #page-wrapper -->
<!-- js placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery-2.1.4.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/metisMenu.min.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>
<!--Morris Chart-->
<script src="assets/plugins/morris/morris.min.js"></script>
<script src="assets/plugins/raphael/raphael-min.js"></script>
<!-- Dashboard init -->
<script src="assets/pages/jquery.dashboard.js"></script>
<!-- Sweet Alert -->
<link href="assets/plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<!-- Sweet-Alert  -->
<script src="assets/plugins/sweet-alert2/sweetalert2.min.js"></script>
<script src="assets/pages/jquery.sweet-alert.init.js"></script>
<!-- App Js -->
<script src="assets/js/jquery.app.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#btnNew").click(function(){
            window.location.href="AddDistributor.php";
        });

    });
function deletePopUp(){
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#4fa7f3',
        cancelButtonColor: '#d57171',
        confirmButtonText: 'Yes, Conform!'
    }).then(function () {
        swal(
            'Deleted!',
            'Successfully Deleted.',
            'success'
        )
    })
}

function editPopUp(){
    swal({
        title: 'Are you sure want to edit?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#4fa7f3',
        cancelButtonColor: '#d57171',
        confirmButtonText: 'Yes, Conform!'
    }).then(function () {
        window.location.href="AddDistributor.php";
    })
}
</script>
</body>
</html>