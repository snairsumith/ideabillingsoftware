<!DOCTYPE html>
<html>
    <head>
        
        <meta charset="utf-8" />
        <title>Billings </title>
        <meta namInvoiceView.php="view Invoiceport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="assets/plugins/morris/morris.css">
        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <!-- MetisMenu CSS -->
        <link href="assets/css/metisMenu.min.css" rel="stylesheet">
        <!-- Icons CSS -->
        <link href="assets/css/icons.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="assets/css/style.css" rel="stylesheet">
        <link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
        #tbl-report{
        display:none;
        }
        </style>
    </head>
    <body>
        <div id="page-wrapper">
            
            <?php include "includes/header.php"; ?>
            <!-- Page content start -->
            <div class="page-contentbar">
                <?php include "includes/side-menu.php"; ?>
                <!-- START PAGE CONTENT -->
                <div id="page-right-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="m-b-20 header-title"><b>Bulk Export Invoice</b></h4>
                                <form>
                                    <div class="form-group">
                                        <label class="control-label">Upload File</label>
                                        <input type="file" class="filestyle" data-buttonname="btn-default">
                                    </div>
                                    
                                    <div class="form-group">
                                        <button type="button" class="btn btn-success btn-bordered" id="btnUpload">Upload</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!--end row -->
                        <div class="row" id="tbl-report">
                            <table class="table table-bordered m-0" >
                                <thead>
                                    <tr>
                                        <th>Vendor</th>
                                        <th>PO No</th>
                                        <th>ASN</th>
                                        <th>Invoice No</th>
                                        <th>Invoice Date</th>
                                        <th>Item Code</th>
                                        <th>HSN Code</th>
                                        <th>Item Description</th>
                                        <th>Qty</th>
                                        <th>MRN</th>
                                        <th>MRN Date</th>
                                        <th>MRN Time</th>
                                        <th>Value</th>
                                        <th>No Of Box</th>
                                        <th>Unit Price</th>
                                        <th>Category</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td scope="row">MEMPHIS PRINITNG & PUBLISHING PVT LTD</td>
                                        <td>11817402120</td>
                                        <td>207746-142080</td>
                                        <td>1916</td>
                                        <td>12/30/2017</td>
                                        <td>NA</td>
                                        <td>MT005017</td>
                                        <td>456853</td>
                                        <td>POP Items</td>
                                        <td>40000</td>
                                        <td>3:15PM</td>
                                        <td>118175211648</td>
                                        <td>12/30/2017</td>
                                        <td>3:30PM</td>
                                        <td>76160</td>
                                        <td>40</td>
                                        <td>3.10</td>
                                        <td>MARKETING</td>

                                        
                                    </tr>
                                     <tr>
                                        <td scope="row">MEMPHIS PRINITNG & PUBLISHING PVT LTD</td>
                                        <td>11817402120</td>
                                        <td>207746-142080</td>
                                        <td>1916</td>
                                        <td>12/30/2017</td>
                                        <td>NA</td>
                                        <td>MT005017</td>
                                        <td>456853</td>
                                        <td>POP Items</td>
                                        <td>40000</td>
                                        <td>3:15PM</td>
                                        <td>118175211648</td>
                                        <td>12/30/2017</td>
                                        <td>3:30PM</td>
                                        <td>76160</td>
                                        <td>40</td>
                                        <td>3.10</td>
                                        <td>MARKETING</td>
                                     
                                        
                                    </tr>
                                     <tr>
                                        <td scope="row">MEMPHIS PRINITNG & PUBLISHING PVT LTD</td>
                                        <td>11817402120</td>
                                        <td>207746-142080</td>
                                        <td>1916</td>
                                        <td>12/30/2017</td>
                                        <td>NA</td>
                                        <td>MT005017</td>
                                        <td>456853</td>
                                        <td>POP Items</td>
                                        <td>40000</td>
                                        <td>3:15PM</td>
                                        <td>118175211648</td>
                                        <td>12/30/2017</td>
                                        <td>3:30PM</td>
                                        <td>76160</td>
                                        <td>40</td>
                                        <td>3.10</td>
                                        <td>MARKETING</td>
                                     
                                        
                                    </tr>
                                     <tr>
                                        <td scope="row">MEMPHIS PRINITNG & PUBLISHING PVT LTD</td>
                                        <td>11817402120</td>
                                        <td>207746-142080</td>
                                        <td>1916</td>
                                        <td>12/30/2017</td>
                                        <td>NA</td>
                                        <td>MT005017</td>
                                        <td>456853</td>
                                        <td>POP Items</td>
                                        <td>40000</td>
                                        <td>3:15PM</td>
                                        <td>118175211648</td>
                                        <td>12/30/2017</td>
                                        <td>3:30PM</td>
                                        <td>76160</td>
                                        <td>40</td>
                                        <td>3.10</td>
                                        <td>MARKETING</td>
                                     
                                        
                                    </tr>
                                     <tr>
                                        <td scope="row">MEMPHIS PRINITNG & PUBLISHING PVT LTD</td>
                                        <td>11817402120</td>
                                        <td>207746-142080</td>
                                        <td>1916</td>
                                        <td>12/30/2017</td>
                                        <td>NA</td>
                                        <td>MT005017</td>
                                        <td>456853</td>
                                        <td>POP Items</td>
                                        <td>40000</td>
                                        <td>3:15PM</td>
                                        <td>118175211648</td>
                                        <td>12/30/2017</td>
                                        <td>3:30PM</td>
                                        <td>76160</td>
                                        <td>40</td>
                                        <td>3.10</td>
                                        <td>MARKETING</td>
                                     
                                        
                                    </tr>
                                     <tr>
                                        <td scope="row">MEMPHIS PRINITNG & PUBLISHING PVT LTD</td>
                                        <td>11817402120</td>
                                        <td>207746-142080</td>
                                        <td>1916</td>
                                        <td>12/30/2017</td>
                                        <td>NA</td>
                                        <td>MT005017</td>
                                        <td>456853</td>
                                        <td>POP Items</td>
                                        <td>40000</td>
                                        <td>3:15PM</td>
                                        <td>118175211648</td>
                                        <td>12/30/2017</td>
                                        <td>3:30PM</td>
                                        <td>76160</td>
                                        <td>40</td>
                                        <td>3.10</td>
                                        <td>MARKETING</td>
                                     
                                        
                                    </tr>
                                     <tr>
                                        <td scope="row">MEMPHIS PRINITNG & PUBLISHING PVT LTD</td>
                                        <td>11817402120</td>
                                        <td>207746-142080</td>
                                        <td>1916</td>
                                        <td>12/30/2017</td>
                                        <td>NA</td>
                                        <td>MT005017</td>
                                        <td>456853</td>
                                        <td>POP Items</td>
                                        <td>40000</td>
                                        <td>3:15PM</td>
                                        <td>118175211648</td>
                                        <td>12/30/2017</td>
                                        <td>3:30PM</td>
                                        <td>76160</td>
                                        <td>40</td>
                                        <td>3.10</td>
                                        <td>MARKETING</td>
                                     
                                        
                                    </tr>
                                     <tr>
                                        <td scope="row">MEMPHIS PRINITNG & PUBLISHING PVT LTD</td>
                                        <td>11817402120</td>
                                        <td>207746-142080</td>
                                        <td>1916</td>
                                        <td>12/30/2017</td>
                                        <td>NA</td>
                                        <td>MT005017</td>
                                        <td>456853</td>
                                        <td>POP Items</td>
                                        <td>40000</td>
                                        <td>3:15PM</td>
                                        <td>118175211648</td>
                                        <td>12/30/2017</td>
                                        <td>3:30PM</td>
                                        <td>76160</td>
                                        <td>40</td>
                                        <td>3.10</td>
                                        <td>MARKETING</td>
                                     
                                        
                                    </tr>
                                    
                                </tbody>
                            </table>
                          <!--   <button type="button" class="btn btn-success btn-bordered pull-right m-t-10">Import All Invoice</button> -->
                            </div> <!-- end row -->
                            
                        </div>
                        <!-- end container -->
                        <?php include "includes/footer.php"; ?>
                    </div>
                    <!-- End #page-right-content -->
                </div>
                <!-- end .page-contentbar -->
            </div>
            <!-- End #page-wrapper -->
            <!-- js placed at the end of the document so the pages load faster -->
            <script src="assets/js/jquery-2.1.4.min.js"></script>
            <script src="assets/js/bootstrap.min.js"></script>
            <script src="assets/js/metisMenu.min.js"></script>
            <script src="assets/js/jquery.slimscroll.min.js"></script>
            <!--Morris Chart-->
            <script src="assets/js/jquery-2.1.4.min.js"></script>
            <script src="assets/js/bootstrap.min.js"></script>
            <script src="assets/js/metisMenu.min.js"></script>
            <script src="assets/js/jquery.slimscroll.min.js"></script>
            <script src="assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
            <script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
            <script src="assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
            <script src="assets/plugins/switchery/switchery.min.js"></script>
            <script type="text/javascript" src="assets/plugins/parsleyjs/parsley.min.js"></script>
            <script src="assets/plugins/moment/moment.js"></script>
            <script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
            <script src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
            <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
            <script src="assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
            <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
            <script src="assets/plugins/summernote/summernote.min.js"></script>
            <!-- form advanced init js -->
            <script src="assets/pages/jquery.form-advanced.init.js"></script>
            <link href="assets/plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<!-- Sweet-Alert  -->
<script src="assets/plugins/sweet-alert2/sweetalert2.min.js"></script>
<script src="assets/pages/jquery.sweet-alert.init.js"></script>
            <!-- App Js -->
            <script src="assets/js/jquery.app.js"></script>
            <script type="text/javascript">
            $("#btnUpload").click(function(){
            
            addPopUp();
            
            });

            function addPopUp(){
                swal({
                title: 'Are you sure want upload?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#4fa7f3',
                cancelButtonColor: '#d57171',
                confirmButtonText: 'Yes, Conform!'
            }).then(function () {
                $("#tbl-report").show(500);
                // window.location.href="Suppliers.php";
            })
            }

            </script>
        </body>
    </html>