<!DOCTYPE html>
<html>
	<head>
		
		<meta charset="utf-8" />
		<title>Billings </title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
		<meta content="Coderthemes" name="author" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<link rel="shortcut icon" href="assets/images/favicon.ico">
		<!--Morris Chart CSS -->
		<link rel="stylesheet" href="assets/plugins/morris/morris.css">
		<!-- Bootstrap core CSS -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<!-- MetisMenu CSS -->
		<link href="assets/css/metisMenu.min.css" rel="stylesheet">
		<!-- Icons CSS -->
		<link href="assets/css/icons.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="assets/css/style.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
		
	</head>
	<body>
		<div id="page-wrapper">
			
			<?php include "includes/header.php"; ?>
			<!-- Page content start -->
			<div class="page-contentbar">
				<?php include "includes/side-menu.php"; ?>
				<!-- START PAGE CONTENT -->
				<div id="page-right-content">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="col-md-12 m-b-20">
									<div class="row">
										<div class="col-md-12">
											<h4 class="m-b-20"><b>Item Report</b></h4>
										</div>
										
									</div>
									<div class="row">
										<div class="col-md-10">
											<div class="input-daterange input-group" id="date-range">
												<input type="date" class="form-control" name="start">
												<span class="input-group-addon b-0">to</span>
												<input type="date" class="form-control" name="end">
											</div>
										</div>
										<div class="col-md-2"><button type="button" class="btn btn-info">Get Report</button></div>
										
									</div>
									
									<div class="row m-t-10">
										<div class="btn-group m-b-5 pull-right">
											<button type="button" class="btn btn-success buttons-copy buttons-html5 btn-sm">Excel</button>
											<button type="button" class="btn btn-default buttons-copy buttons-html5 btn-sm">Print</button>
											
										</div>
										<table class="table table-bordered m-0">
											<thead>
												<tr>
													<th>Item Code</th>
													<th>HSN Code</th>
													<th>Item Name</th>
													<th>Purchase Date</th>
													<th>Purchase Quantity</th>
													<th>AGEING</th>
													
												</tr>
											</thead>
											<tbody>
												
												<tr>
													<td scope="row">AD016004</td>
													<td>48193000</td>
													<td>Merchandising Item - Bags</td>
													<td>22-Dec-2017</td>
													<td>100</td>
													<td>25</td>
													
												</tr>
												<tr>
													<td scope="row">HR005017</td>
													<td>59070099</td>
													<td>Banners - Cloth - 10'x3'</td>
													<td>10-Oct-2017</td>
													<td>50</td>
													<td>15</td>
													
												</tr>
											</tbody>
										</table>
										
									</div>
									
								</div>
							</div>
						</div>
						<!--end row -->
						<div class="row"></div> <!-- end row -->
						
					</div>
					<!-- end container -->
					<?php include "includes/footer.php"; ?>
				</div>
				<!-- End #page-right-content -->
			</div>
			<!-- end .page-contentbar -->
		</div>
		<!-- End #page-wrapper -->
		<!-- js placed at the end of the document so the pages load faster -->
		<script src="assets/js/jquery-2.1.4.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/metisMenu.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<!--Morris Chart-->
		<script src="assets/plugins/morris/morris.min.js"></script>
		<script src="assets/plugins/raphael/raphael-min.js"></script>
		<!-- Dashboard init -->
		<script src="assets/pages/jquery.dashboard.js"></script>
		<!-- App Js -->
		<script src="assets/js/jquery.app.js"></script>
		<script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
		<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#btnItemNew").click(function(){
					window.location.href="AddItem.php";
				});
				$("#btnItemExport").click(function(){
					window.location.href="docs/items.xlsx";
				});
			});
		</script>
	</body>
</html>