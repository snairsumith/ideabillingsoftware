<!DOCTYPE html>
<html>
	<head>
		
		<meta charset="utf-8" />
		<title>Billings </title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
		<meta content="Coderthemes" name="author" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<link rel="shortcut icon" href="assets/images/favicon.ico">
		<!--Morris Chart CSS -->
		<link rel="stylesheet" href="assets/plugins/morris/morris.css">
		<!-- Bootstrap core CSS -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<!-- MetisMenu CSS -->
		<link href="assets/css/metisMenu.min.css" rel="stylesheet">
		<!-- Icons CSS -->
		<link href="assets/css/icons.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="assets/css/style.css" rel="stylesheet">
		
	</head>
	<body>
		<div id="page-wrapper">
			
			<?php include "includes/header.php"; ?>
			<!-- Page content start -->
			<div class="page-contentbar">
				<?php include "includes/side-menu.php"; ?>
				<!-- START PAGE CONTENT -->
				<div id="page-right-content">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="col-md-12 m-b-20">
									<div class="row">
										<div class="col-md-9">
											<h4 class="m-b-20"><b>Stock Report</b></h4>
										</div>
										<div class="col-md-3">
											<button type="button" id="btnItemNew" class="btn btn-primary">Add New</button>
											
											<button type="button" id="btnItemExport" class="btn btn-info">Import</button>
										</div>
										
									</div>
									
									
									<div class="row">
										<table class="table table-bordered m-0">

                                                <thead>
                                                    <tr>
                                                        <th>Item Code</th>
                                                        <th>Item Description</th>
                                                        <th>HSN Code</th>
                                                        <th>Unit Price</th>
                                                        <th>Quantity</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td scope="row">ITM100</th>
                                                        <td>Item 1</td>
                                                        <td>8547</td>
                                                        <td>20.12</td>
                                                        <td>2</td>
                                                    </tr>
                                                     <tr>
                                                        <td scope="row">ITM101</th>
                                                        <td>Item 2</td>
                                                        <td>8547</td>
                                                        <td>20.12</td>
                                                        <td>2</td>
                                                    </tr>
                                                     <tr>
                                                        <td scope="row">ITM103</th>
                                                        <td>Item 3</td>
                                                        <td>8547</td>
                                                        <td>20.12</td>
                                                        <td>2</td>
                                                    </tr>
                                                    <tr>
                                                        <td scope="row">ITM104</th>
                                                        <td>Item 4</td>
                                                        <td>8547</td>
                                                        <td>20.12</td>
                                                        <td>2</td>
                                                    </tr>
                                                   	<tr>
                                                        <td scope="row">ITM105</th>
                                                        <td>Item 5</td>
                                                        <td>8547</td>
                                                        <td>20.12</td>
                                                        <td>2</td>
                                                    </tr>
                                                </tbody>
                                            </table>
									</div>
									
								</div>
							</div>
						</div>
						<!--end row -->
						<div class="row"></div> <!-- end row -->
						
					</div>
					<!-- end container -->
					<?php include "includes/footer.php"; ?>
				</div>
				<!-- End #page-right-content -->
			</div>
			<!-- end .page-contentbar -->
		</div>
		<!-- End #page-wrapper -->
		<!-- js placed at the end of the document so the pages load faster -->
		<script src="assets/js/jquery-2.1.4.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/metisMenu.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<!--Morris Chart-->
		<script src="assets/plugins/morris/morris.min.js"></script>
		<script src="assets/plugins/raphael/raphael-min.js"></script>
		<!-- Dashboard init -->
		<script src="assets/pages/jquery.dashboard.js"></script>
		<!-- App Js -->
		<script src="assets/js/jquery.app.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#btnItemNew").click(function(){
					window.location.href="AddItem.php";
				});
				$("#btnItemExport").click(function(){
					window.location.href="docs/items.xlsx";
				});

			});
		</script>
	</body>
</html>