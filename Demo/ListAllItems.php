<!DOCTYPE html>
<html>
	<head>
		
		<meta charset="utf-8" />
		<title>Billings </title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
		<meta content="Coderthemes" name="author" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<link rel="shortcut icon" href="assets/images/favicon.ico">
		<!--Morris Chart CSS -->
		<link rel="stylesheet" href="assets/plugins/morris/morris.css">
		<!-- Bootstrap core CSS -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<!-- MetisMenu CSS -->
		<link href="assets/css/metisMenu.min.css" rel="stylesheet">
		<!-- Icons CSS -->
		<link href="assets/css/icons.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="assets/css/style.css" rel="stylesheet">
		
	</head>
	<body>
		<div id="page-wrapper">
			
			<?php include "includes/header.php"; ?>
			<!-- Page content start -->
			<div class="page-contentbar">
				<?php include "includes/side-menu.php"; ?>
				<!-- START PAGE CONTENT -->
				<div id="page-right-content">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="col-md-12 m-b-20">
									<div class="row">
										<div class="col-md-6">
											<h4 class="m-b-20"><b>All Items</b></h4>
										</div>
										<div class="col-md-6">
											<button type="button" id="btnItemNew" class="btn btn-primary">Add New</button>
											
											<button type="button" id="btnItemExport" class="btn btn-info">Import</button>
											<button type="button" id="btnSearch" class="btn btn-dark">Search Item</button>
										</div>
										
									</div>
									
									
									<div class="row">
										<table class="table table-bordered m-0">

                                                <thead>
                                                    <tr>
                                                        <th>Item Code</th>
                                                        <th>Item Description</th>
                                                        <th>HSN Code</th>
                                                        <th>Unit Price</th>
                                                        <th>Min Quantity</th>
                                                        <th>Max Quantity</th>
                                                        <th>Category</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td scope="row">AD016004</th>
                                                        <td>Merchandising Item - Bags</td>
                                                        <td>48193000</td>
                                                        <td>20.12</td>
                                                        <td>2</td>
                                                        <td>50</td>
                                                        <td>Sales</td>

                                                        <td>
                                                            <a href="#">Edit</a> /
                                                            <a href="#">Delete</a>
                                                        </td>
                                                    </tr>
                                                     <tr>
                                                        <td scope="row">AD016005</th>
                                                        <td>Merchandising Item - Pens</td>
                                                        <td>96081019</td>
                                                        <td>20.12</td>
                                                        <td>2</td>
                                                        <td>50</td>
                                                        <td>Sales</td>
                                                        <td>
                                                            <a href="#">Edit</a> /
                                                            <a href="#">Delete</a>
                                                        </td>
                                                    </tr>
                                                     <tr>
                                                        <td scope="row">HR004001</th>
                                                        <td>POP Items - Danglers</td>
                                                        <td>49111010</td>
                                                        <td>20.12</td>
                                                        <td>2</td>
                                                        <td>50</td>
                                                        <td>Sales</td>
                                                        <td>
                                                            <a href="#">Edit</a> /
                                                            <a href="#">Delete</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td scope="row">MT00258</th>
                                                        <td>Banners - Cloth, Size 3x2 , 4 colour , single side printing  4 eyelets with Tag</td>
                                                        <td>59070099</td>
                                                        <td>20.12</td>
                                                        <td>2</td>
                                                        <td>50</td>
                                                        <td>Sales</td>
                                                        <td>
                                                            <a href="#">Edit</a> /
                                                            <a href="#">Delete</a>
                                                        </td>
                                                    </tr>
                                                   	
                                                </tbody>
                                            </table>
									</div>
									
								</div>
							</div>
						</div>
						<!--end row -->
						<div class="row"></div> <!-- end row -->
						
					</div>
					<!-- end container -->
					<?php include "includes/footer.php"; ?>
				</div>
				<!-- End #page-right-content -->
			</div>
			<!-- end .page-contentbar -->
		</div>
		<!-- End #page-wrapper -->
		<!-- js placed at the end of the document so the pages load faster -->
		<script src="assets/js/jquery-2.1.4.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/metisMenu.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<!--Morris Chart-->
		<script src="assets/plugins/morris/morris.min.js"></script>
		<script src="assets/plugins/raphael/raphael-min.js"></script>
		<!-- Dashboard init -->
		<script src="assets/pages/jquery.dashboard.js"></script>
		<!-- App Js -->
		<script src="assets/js/jquery.app.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#btnItemNew").click(function(){
					window.location.href="AddItem.php";
				});
				$("#btnItemExport").click(function(){
					window.location.href="docs/items.xlsx";
				});
				$("#btnSearch").click(function(){
					window.location.href="itemSerach.php";
				});

			});
		</script>
	</body>
</html>