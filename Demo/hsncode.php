<!DOCTYPE html>
<html>
	<head>
		
		<meta charset="utf-8" />
		<title>Billings </title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
		<meta content="Coderthemes" name="author" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<link rel="shortcut icon" href="assets/images/favicon.ico">
		<!--Morris Chart CSS -->
		<link rel="stylesheet" href="assets/plugins/morris/morris.css">
		<!-- Bootstrap core CSS -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<!-- MetisMenu CSS -->
		<link href="assets/css/metisMenu.min.css" rel="stylesheet">
		<!-- Icons CSS -->
		<link href="assets/css/icons.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="assets/css/style.css" rel="stylesheet">
		
	</head>
	<body>
		<div id="page-wrapper">
			
			<?php include "includes/header.php"; ?>
			<!-- Page content start -->
			<div class="page-contentbar">
				<?php include "includes/side-menu.php"; ?>
				<!-- START PAGE CONTENT -->
				<div id="page-right-content">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="col-md-12 m-b-20">
									<div class="row">
										<div class="col-md-9">
											<h4 class="m-b-20"><b>HSN Code Details</b></h4>
										</div>
										<div class="col-md-3">
											<button type="button" data-toggle="modal" data-target="#con-close-modal" class="btn btn-primary pull-right">Add New</button>
											
											
										</div>
										
									</div>
									
									
									<div class="row">
										<table class="table table-bordered m-0">

                                                <thead>
                                                    <tr>
                                                        <th>HSN Id</th>
                                                        <th>HSN Code</th>
                                                        <th>Tax Percentage</th>
                                                        <th>Iteam Code</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td scope="row">1</th>
                                                        <td>48193000</td>
                                                        <td>12%</td>
                                                        <td>MT005017</td>
                                                        <td>
                                                            <a href="#">Edit</a> /
                                                            <a href="#">Delete</a>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td scope="row">2</th>
                                                        <td>96081019</td>
                                                        <td>12%</td>
                                                        <td>MT003046</td>
                                                        <td>
                                                            <a href="#">Edit</a> /
                                                            <a href="#">Delete</a>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td scope="row">3</th>
                                                        <td>73218990</td>
                                                        <td>28%</td>
                                                        <td>MT003009</td>
                                                        <td>
                                                            <a href="#">Edit</a> /
                                                            <a href="#">Delete</a>
                                                        </td>
                                                        
                                                    </tr>
                                                    
                                                     
                                                </tbody>
                                            </table>
									</div>
									
								</div>
							</div>
						</div>
						<!--end row -->
						<div class="row"></div> <!-- end row -->
						
					</div>
					<!-- end container -->
					<?php include "includes/footer.php"; ?>
				</div>
				<!-- End #page-right-content -->
			</div>
			<!-- end .page-contentbar -->
		</div>
		<!-- End #page-wrapper -->
		<!-- js placed at the end of the document so the pages load faster -->

        <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Add new HSN Code</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">HSN Code</label>
                                                                <input type="text" class="form-control" id="field-1" placeholder="HSN Code">
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Tax Percentage</label>
                                                                <input type="text" class="form-control" id="field-1" placeholder="percentage">
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Item Code</label>
                                                                <input type="text" class="form-control" id="field-1" placeholder="Iteam Code">
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                    
                                                    
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
		<script src="assets/js/jquery-2.1.4.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/metisMenu.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<!--Morris Chart-->
		<script src="assets/plugins/morris/morris.min.js"></script>
		<script src="assets/plugins/raphael/raphael-min.js"></script>
		<!-- Dashboard init -->
		<script src="assets/pages/jquery.dashboard.js"></script>
		<!-- App Js -->
		<script src="assets/js/jquery.app.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#btnNew").click(function(){
					window.location.href="AddSupplier.php";
				});

			});
		</script>
	</body>
</html>