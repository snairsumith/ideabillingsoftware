<!DOCTYPE html>
<html>
	<head>
		
		<meta charset="utf-8" />
		<title>Billings </title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
		<meta content="Coderthemes" name="author" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<link rel="shortcut icon" href="assets/images/favicon.ico">
		<!--Morris Chart CSS -->
		<link rel="stylesheet" href="assets/plugins/morris/morris.css">
		<!-- Bootstrap core CSS -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<!-- MetisMenu CSS -->
		<link href="assets/css/metisMenu.min.css" rel="stylesheet">
		<!-- Icons CSS -->
		<link href="assets/css/icons.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="assets/css/style.css" rel="stylesheet">
	</head>
	<body>
		<div id="page-wrapper">
			
			<?php include "includes/header.php"; ?>
			<!-- Page content start -->
			<div class="page-contentbar">
				<?php include "includes/side-menu.php"; ?>
				<!-- START PAGE CONTENT -->
				<div id="page-right-content">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="col-md-12 m-b-20">
                                        <h4 class="m-b-20 header-title"><b>Add Supplier</b></h4>
                                        
                                        <form role="form">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Supplier Code</label>
                                                <input type="text" class="form-control" id="exampleInputEmail1" readonly value="SUP100" placeholder="Text Here">
                                            </div>
                                             <div class="form-group">
                                                <label for="exampleInputEmail1">Supplier Name</label>
                                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Text Here">
                                            </div>
                                             <div class="form-group">
                                                <label for="exampleInputEmail1">Email address</label>
                                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Text Here">
                                            </div>
                                             <div class="form-group">
                                                <label for="exampleInputEmail1">Phone No</label>
                                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Text Here">
                                            </div>
                                             <div class="form-group">
                                                <label for="exampleInputEmail1">Mobile No</label>
                                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Text Here">
                                            </div>
                                             <div class="form-group">
                                                <label for="exampleInputEmail1">Zone</label>
                                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Text Here">
                                            </div>
                                             <div class="form-group">
                                                <label for="exampleInputEmail1">Address</label>
                                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Text Here">
                                            </div>
                                           
                                            
                                            <button type="button" class="btn btn-primary" onclick="addPopUp();" 
>Submit</button>
                                        </form>
                                    </div>
							</div>
						</div>
						<!--end row -->
						<div class="row"></div> <!-- end row -->
						
					</div>
					<!-- end container -->
					<?php include "includes/footer.php"; ?>
				</div>
				<!-- End #page-right-content -->
			</div>
			<!-- end .page-contentbar -->
		</div>
		<!-- End #page-wrapper -->
		<!-- js placed at the end of the document so the pages load faster -->
		<script src="assets/js/jquery-2.1.4.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/metisMenu.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<!--Morris Chart-->
		<script src="assets/plugins/morris/morris.min.js"></script>
		<script src="assets/plugins/raphael/raphael-min.js"></script>
		<!-- Dashboard init -->
		<script src="assets/pages/jquery.dashboard.js"></script>

		       <!-- Sweet Alert -->
        <link href="assets/plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">

   <!-- Sweet-Alert  -->
        <script src="assets/plugins/sweet-alert2/sweetalert2.min.js"></script>
        <script src="assets/pages/jquery.sweet-alert.init.js"></script>
		<!-- App Js -->
		<script src="assets/js/jquery.app.js"></script>


		<script type="text/javascript">
			function addPopUp(){
				swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#4fa7f3',
                cancelButtonColor: '#d57171',
                confirmButtonText: 'Yes, Conform!'
            }).then(function () {
                swal(
                    'Stored!',
                    'Successfully Stored.',
                    'success'
                )
                // window.location.href="Suppliers.php";
            })
			}
		</script>
	</body>
</html>