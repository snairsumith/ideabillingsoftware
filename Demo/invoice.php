<!DOCTYPE html>
<html>
    <head>
        
        <meta charset="utf-8" />
        <title>Billings </title>
        <meta namInvoiceView.php="view Invoiceport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="assets/plugins/morris/morris.css">
        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <!-- MetisMenu CSS -->
        <link href="assets/css/metisMenu.min.css" rel="stylesheet">
        <!-- Icons CSS -->
        <link href="assets/css/icons.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="assets/css/style.css" rel="stylesheet">
        <link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
        #tbl-report{
        display:none;
        }
        </style>
    </head>
    <body>
        <div id="page-wrapper">
            
            <?php include "includes/header.php"; ?>
            <!-- Page content start -->
            <div class="page-contentbar">
                <?php include "includes/side-menu.php"; ?>
                <!-- START PAGE CONTENT -->
                <div id="page-right-content">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-12">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h2 class="text-center">TAX INVOICE</h2>
                                        </div>
                                        <div class="col-md-2">
                                            <img src="assets/images/logo.png" alt="" height="40">
                                        </div>
                                    </div>
                                    <div class="row m-t-30">
                                        <div class="col-md-4">
                                            <h5>Deatils Of Supplier</h5>
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <div class="col-md-6"> Supplier Name</div>
                                                    <div class="col-md-6">
                                                        Idea Cellular Ware House
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6"> Supplier Address</div>
                                                    <div class="col-md-6">
                                                        Idea Cellular Ware House,X/222,
                                                        jublee Road,Neetoor,Cochin
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6"> Supplier State</div>
                                                    <div class="col-md-6">
                                                        Kerala
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6"> Invoice No</div>
                                                    <div class="col-md-6">
                                                        WH/NT/011
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6"> Invoice Date</div>
                                                    <div class="col-md-6">
                                                        22-Dec-2017
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6"> Supplier State</div>
                                                    <div class="col-md-6">
                                                        Kerala
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6"> Supplier State Code</div>
                                                    <div class="col-md-6">
                                                        32
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <div class="col-md-6"> Supplier GSTIN</div>
                                                    <div class="col-md-6">
                                                        32AF080808JHGK
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                        </div>
                                        <div class="col-md-4">
                                            <h5>Deatils Of Consignee</h5>
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <div class="col-md-6">Name & Address</div>
                                                    <div class="col-md-6">
                                                       Mass Communications, 17/44 Zam Zam Building Near Moffusil Bus stand Mavoor Road Calicut,673001
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <div class="col-md-6">Contact No</div>
                                                    <div class="col-md-6">
                                                        9878454545
                                                    </div>
                                                </div>
                                              
                                                <div class="form-group">
                                                    <div class="col-md-6">  State</div>
                                                    <div class="col-md-6">
                                                        Kerala
                                                    </div>
                                                </div>
                                                
                                                 <div class="form-group">
                                                    <div class="col-md-6"> Supplier GSTIN</div>
                                                    <div class="col-md-6">
                                                        32AF080808JHGK
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6"> Place of Supply</div>
                                                    <div class="col-md-6">
                                                        Calicut
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        </div>
                                    </div>
                                    <div class="row m-t-10">
                                        <table class="table table-bordered m-0">

                                                <thead>
                                                    <tr>
                                                        <th>S.No</th>
                                                        <th>Item Code</th>
                                                        <th>Description</th>
                                                        <th>HSN</th>
                                                        <th>UOM</th>
                                                        <th>QTY</th>
                                                        <th>Rate</th>
                                                        <th>Tax%</th>
                                                        <th>Total Value</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">1</th>
                                                        <td>MT005017</td>
                                                        <td>POP Items - Chart Sheet </td>
                                                        <td>456853</td>
                                                        <td>Nos</td>
                                                        <td>100.00</td>
                                                        <td>1,633.00</td>
                                                        <td></td>
                                                        <td>163,300.00</td>
                                                    </tr>
                                                    
                                                <tfoot>
                                                    <tr>
                                                        <th scope="row"></th>
                                                        <td></td>
                                                        <td>IGST </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>18.00</td>
                                                        <td>29,394</td>
                                                    </tr>
                                                </tfoot>
                                                </tbody>
                                            </table>
                                    </div>
                                    <div class="row m-t-10">
                                        <h4 class="text-center">Invoice Summery</h4>
                                        <table class="table table-bordered m-0">

                                                <thead>
                                                    <tr>
                                                        
                                                        <th>Value</th>
                                                        <th>CGST(Rs.)</th>
                                                        <th>SGST/UTGST(Rs.)</th>
                                                        <th>IGST(Rs)</th>
                                                        <th>Total Invoice Value(Rs)</th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        
                                                        <td>163,300.00</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>29,394</td>
                                                        <td>192,394.00</td>
                                                    </tr>
                                                    
                                                
                                                </tbody>
                                            </table>
                                        
                                    </div>
                                    <div class="row m-t-10">
                                        <h4 class="text-center">Goods Dispatch Details</h4>
                                        <table class="table table-bordered m-0">

                                                <thead>
                                                    <tr>
                                                        
                                                        <th>Transporter Name</th>
                                                        <th></th>
                                                        <th>No Of Packages</th>
                                                        <th></th>
                                                        
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        
                                                        <td>Vechile No</td>
                                                        <td></td>
                                                        <td>Weight</td>
                                                        <td></td>
                                                        
                                                    </tr>
                                                    
                                                
                                                </tbody>
                                            </table>
                                        
                                    </div>
                                    <div class="row m-t-10">
                                        <div class="col-md-3">
                                            <p>(Prepared By)</p>
                                            <p>Idea Tele System</p>
                                        </div>
                                        <div class="col-md-3">
                                             <p>(Verified By)</p>
                                            <p>Idea Tele System</p>
                                        </div>
                                        <div class="col-md-3">
                                             <p>(Authorized By)</p>
                                            <p>Idea Tele System</p>
                                        </div>
                                        <div class="col-md-3">
                                             <p>(Recived By)</p>
                                            <p>Name & Signature<br>Mobile No<br>Idea Tele Syst</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <!--end row -->
                        <div class="row"></div> <!-- end row -->
                        
                    </div>
                    <!-- end container -->
                    <?php include "includes/footer.php"; ?>
                </div>
                <!-- End #page-right-content -->
            </div>
            <!-- end .page-contentbar -->
        </div>
        <!-- End #page-wrapper -->
        <!-- js placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery-2.1.4.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/jquery.slimscroll.min.js"></script>
        <!--Morris Chart-->
        <script src="assets/js/jquery-2.1.4.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/jquery.slimscroll.min.js"></script>
        <script src="assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
        <script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
        <script src="assets/plugins/switchery/switchery.min.js"></script>
        <script type="text/javascript" src="assets/plugins/parsleyjs/parsley.min.js"></script>
        <script src="assets/plugins/moment/moment.js"></script>
        <script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
        <script src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
        <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
        <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/summernote/summernote.min.js"></script>
        <!-- form advanced init js -->
        <script src="assets/pages/jquery.form-advanced.init.js"></script>
        <!-- App Js -->
        <script src="assets/js/jquery.app.js"></script>
        <script type="text/javascript">
        $("#btnUpload").click(function(){
        $("#tbl-report").show(500);
        
        });
        </script>
    </body>
</html>