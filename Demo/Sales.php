<!DOCTYPE html>
<html>
	<head>
		
		<meta charset="utf-8" />
		<title>Billings </title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
		<meta content="Coderthemes" name="author" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<link rel="shortcut icon" href="assets/images/favicon.ico">
		<!--Morris Chart CSS -->
		<link rel="stylesheet" href="assets/plugins/morris/morris.css">
		<!-- Bootstrap core CSS -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<!-- MetisMenu CSS -->
		<link href="assets/css/metisMenu.min.css" rel="stylesheet">
		<!-- Icons CSS -->
		<link href="assets/css/icons.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="assets/css/style.css" rel="stylesheet">
		<link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			#div-supplier-deatils{
				display:none;
			}
		</style>
	</head>
	<body>
		<div id="page-wrapper">
			
			<?php include "includes/header.php"; ?>
			<!-- Page content start -->
			<div class="page-contentbar">
				<?php include "includes/side-menu.php"; ?>
				<!-- START PAGE CONTENT -->
				<div id="page-right-content">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="col-md-12 m-b-20">
                                        <h4 class="m-b-20 header-title"><b>New Outward</b></h4>
                                    <div class="row m-b-10">
                                    	<div class="col-md-4">
                                    		<div id="div-supplier-select">
                                    		<h6 class="text-muted">Select Customer</h6>
                                    		<select class="form-control select2" id="cmbSup">
                                        		<option>Select</option>
                                        		<option value="AZ">Mass Communications</option>
                                           
                                        	</select>
                                        	</div>
                                        	<div id="div-supplier-deatils">
                                        		<h3>Mass Communications </h3>
                                        		<p class="text-muted">17/44 Zam Zam Building<br> Near Moffusil Bus stand Mavoor Road</p>
                                        		 <p class="text-muted"> Calicut,673001</p>
                                        	</div>

                                    	</div>
                                    	<div class="col-md-4"></div>
                                    	<div class="col-md-4">
                                    		<div class="form-group">
                                                <h6 class="text-muted">Invoice No</h6>
                                                <div class="col-sm-12">
                                                    <input type="text" id="example-input-normal" name="example-input-normal" class="form-control" value="45783232">
                                                </div>
                                            </div>
                                            <div class="form-group m-b-5">
                                                <h6 class="text-muted">Invoice Date</h6>
                                                <div class="col-sm-12">
                                                    <input type="text" id="example-input-normal" name="example-input-normal" class="form-control" value="12-Nov-2017">
                                                </div>
                                            </div>
                                            <div class="row">
                                            	<div class="col-md-4">
                                            		<button type="button" class="btn btn-success btn-bordered" id="btnInvoice">Save</button>
                                            	</div>
                                            	
                                            </div>
                                    	</div>
                                    </div>    	
                                    <hr class="header-title">
                                    <div class="row m-t-10">
                                    	<div class="col-md-4">
                                    		<select class="form-control select2" id="cmbItem">
                                        		<option>Select</option>
                                        		<option value="AZ">Merchandising Item - Bags</option>
                                            <option value="CO">POP Items - Danglers</option>
                                            <option value="ID">Banners - Cloth - 10'x3'
</option>
                                            
                                           
                                        	</select>
                                    	</div>
                                    	<div class="col-md-2">
                                    		<input type="text" name="" class="form-control" placeholder="Quantity">	
                                    	</div>
                                    	<div class="col-md-2">
                                    		<input type="text" name="" class="form-control" placeholder="Tax">	
                                    	</div>
                                    	<div class="col-md-2">
                                    		<input type="text" name="" class="form-control" readonly value="10.00">
                                    	</div>
                                    	<div class="col-md-2">
                                    		<button type="button" class="btn btn-success btn-bordered">Add New</button>
                                    	</div>
                                    </div>
                                    <hr class="header-title">
                                    <div class="row m-t-10">
                                    	<table class="table table-bordered m-0">

                                                <thead>
                                                    <tr>
                                                    	<th>S.No</th>
                                                        <th>Item Code</th>
                                                        <th>Description</th>
                                                        <th>HSN Code</th>
                                                        <th>Qty</th>
                                                        <th>Rate</th>
                                                        <th>Tax %</th>
                                                        <th>Total Value</th>
                                                        <th>Action</th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td scope="row">1</td>
                                                        <td>AD016004
</td>
                                                        <td>Merchandising Item - Bags</td>
                                                        <td>48193000</td>
                                                        <td>100</td>
                                                        <td>1,889.75</td>
                                                        <td>12%</td>
                                                        <td>188,9747.66</td>
                                                        <td>
                                                            
                                                            <a href="#">Delete</a>
                                                        </td>
                                                    </tr>
                                                </tbody>

                                        </table>
                                        </td>
                                    </div>

                                </div>
							</div>
						</div>
						<!--end row -->
						<div class="row"></div> <!-- end row -->
						
					</div>
					<!-- end container -->
					<?php include "includes/footer.php"; ?>
				</div>
				<!-- End #page-right-content -->
			</div>
			<!-- end .page-contentbar -->
		</div>
		<!-- End #page-wrapper -->
		<!-- js placed at the end of the document so the pages load faster -->
		<script src="assets/js/jquery-2.1.4.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/metisMenu.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<!--Morris Chart-->
		 <script src="assets/js/jquery-2.1.4.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/jquery.slimscroll.min.js"></script>

        <script src="assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
        <script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
        <script src="assets/plugins/switchery/switchery.min.js"></script>
        <script type="text/javascript" src="assets/plugins/parsleyjs/parsley.min.js"></script>

        <script src="assets/plugins/moment/moment.js"></script>
     	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
     	<script src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
     	<script src="assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/summernote/summernote.min.js"></script>
        <!-- form advanced init js -->
        <script src="assets/pages/jquery.form-advanced.init.js"></script>

        <link href="assets/plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<!-- Sweet-Alert  -->
<script src="assets/plugins/sweet-alert2/sweetalert2.min.js"></script>
<script src="assets/pages/jquery.sweet-alert.init.js"></script>

        <!-- App Js -->
        <script src="assets/js/jquery.app.js"></script>
        <script type="text/javascript">
        	$("#cmbSup").change(function(){
        		$("#div-supplier-deatils").show();
        		$("#cmbSup").hide();
        	});
            $("#btnInvoice").click(function(){
                addPopUp();
            });

            function addPopUp(){
                swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#4fa7f3',
                cancelButtonColor: '#d57171',
                confirmButtonText: 'Yes, Conform!'
            }).then(function () {
               window.location.href="InvoiceView.php";
                // window.location.href="Suppliers.php";
            })
            }
        </script>


	</body>
</html>