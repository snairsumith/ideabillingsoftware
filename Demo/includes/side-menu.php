left navigation start-->
<aside class="sidebar-navigation">
  <div class="scrollbar-wrapper">
    <div>
      <button type="button" class="button-menu-mobile btn-mobile-view visible-xs visible-sm">
      <i class="mdi mdi-close"></i>
      </button>
      <!-- User Detail box -->
      <div class="user-details">
        <div class="pull-left">
          <img src="assets/images/users/img_avatar.png" alt="" class="thumb-md img-circle">
        </div>
        <div class="user-info">
          <a href="#">Pradosh</a>
          <p class="text-muted m-0">Administrator</p>
        </div>
      </div>
      <!--- End User Detail box -->
      <!-- Left Menu Start -->
      <ul class="metisMenu nav" id="side-menu">
        <!-- <li><a href="Dashboard.php"><i class="ti-dashboard"></i> Dashboard </a></li> -->
        <li><a href="Distributors.php"><i class="ti-dashboard"></i> Distributer </a></li>
       <!--  <li>
          <a href="javascript: void(0);" aria-expanded="true">
            <i class="ti-user"></i> Contacts <span class="fa arrow"></span>
          </a>
          <ul class="nav-second-level nav" aria-expanded="true">
            <li><a href="Suppliers.php">Supplier</a></li>
            <li><a href="Distributors.php">Distributer</a></li>
          </ul>
        </li> -->
        <li>
          <a href="javascript: void(0);" aria-expanded="true">
            <i class="ti-view-list-alt"></i> Transactions <span class="fa arrow"></span>
          </a>
          <ul class="nav-second-level nav" aria-expanded="true">
            <li><a href="Sales.php">Single Outward</a></li>
            <li><a href="importInvoice.php">Bulk Outward</a></li>
            <li><a href="Purchase.php">Single Inward</a></li>
            <li><a href="importInvoice.php">Bulk Inward</a></li>

          </ul>
        </li>
       <!--   <li><a href="ImportMRN.php"><i class="ti-dashboard"></i> Import MRN </a></li>
         <li><a href="ListAllItems.php"><i class="ti-printer"></i> Items </a></li> -->
          <li><a href="hsncode.php"><i class="ti-printer"></i> HSN Code </a></li>
        <li>
          <a href="javascript: void(0);" aria-expanded="true">
            <i class="ti-agenda"></i> Report <span class="fa arrow"></span>
          </a>
          <ul class="nav-second-level nav" aria-expanded="true">
               <li><a href="ItemReport.php">Stock Report</a></li>
            <li><a href="SalesReport.php">Inward</a></li>
            <li><a href="PurchaseReport.php">Outward</a></li>
            
          </ul>
        </li>
       
      
        <li><a href="#"><i class="ti-power-off"></i> Logout </a></li>
      </ul>
      
    </div>
    </div><!--Scrollbar wrapper-->
  </aside>
  <!--left navigation end