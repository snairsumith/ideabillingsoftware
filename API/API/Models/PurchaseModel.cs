﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class PurchaseModel
    {
        public int PurchaseId { get; set; }
        public int SupplierId { get; set; }
        public string InvoiceNo { get; set; }
        public System.DateTime InvoiceDate { get; set; }
        public System.DateTime InvoiceDueDate { get; set; }
        public string PaymentStatus { get; set; }
        public decimal GrandTotalAmount { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public System.DateTime UpdatedOn { get; set; }
    }
}