﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class BillingModel
    {
        public int InvoiceId { get; set; }
        public int CustomerId { get; set; }
        public string BillNo { get; set; }
        public System.DateTime BillDate { get; set; }
        public string PaymentStatus { get; set; }
        public decimal TotalAmount { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public System.DateTime UpdatedOn { get; set; }
    }
}