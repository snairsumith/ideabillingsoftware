﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class PurchaseBillingItemModel
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public int PurchaseId { get; set; }
        public string ItemTax { get; set; }
        public decimal ItemPrice { get; set; }
        public int ItemQuantity { get; set; }
        public decimal NetAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public int Type { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public System.DateTime UpdatedOn { get; set; }
    }
}