﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class SupplierModel
    {
        public int SupplierId { get; set; }
        public string Address { get; set; }
        public int LocationId { get; set; }
        public string EmailID { get; set; }
        public string PhoneNumber { get; set; }
        public string GstNo { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public System.DateTime UpdatedOn { get; set; }
    }
}