﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class PurchaseController : ApiController
    {
        private billingEntities DB = new billingEntities();
        [HttpGet]
        public IHttpActionResult GetAllPurchase()
        {
            var result = from purchased in DB.purchases
                         select new
                         {
                             purchased.PurchaseId,
                             purchased.SupplierId,
                             purchased.InvoiceNo,
                             purchased.InvoiceDate,
                             purchased.InvoiceDueDate,
                             purchased.PaymentStatus,
                             purchased.GrandTotalAmount,
                             purchased.CreatedOn,
                             purchased.UpdatedOn,
                         };
            return Ok(result);

        }
        [HttpGet]
        public IHttpActionResult GetById(int Id)
        {
            var result = from purchased in DB.purchases
                         where purchased.PurchaseId.Equals(Id)
                         select new
                         {
                             purchased.PurchaseId,
                             purchased.SupplierId,
                             purchased.InvoiceNo,
                             purchased.InvoiceDate,
                             purchased.InvoiceDueDate,
                             purchased.PaymentStatus,
                             purchased.GrandTotalAmount,
                             purchased.CreatedOn,
                             purchased.UpdatedOn,
                         };
            return Json(result);
        }
        [HttpPost]
        public IHttpActionResult InsertPurchaseDetails(PurchaseModel pm)
        {
            //if (ss.LocationId == null)
            //{
            //    return Json("location required");
            //}
            int result = 0;
            using (var ctx = new billingEntities())
            {
                ctx.purchases.Add(new purchase()
                {
                    SupplierId = pm.SupplierId,
                    InvoiceNo = pm.InvoiceNo,
                    InvoiceDate = DateTime.Now,
                    InvoiceDueDate = DateTime.Now,
                    PaymentStatus = pm.PaymentStatus,
                    GrandTotalAmount = pm.GrandTotalAmount,
                    UpdatedOn = DateTime.Now,
                    CreatedOn = DateTime.Now,
                });
                result = ctx.SaveChanges();
            }
            if (result > 0)
            {
                return Json("{resultCode:1,message:sucess}");
            }
            else
            {
                return Json("{resultCode:0,message:faill}");
            }
        }
        [HttpPost]
        public IHttpActionResult InsertPurchaseBillingItemsDetails(PurchaseBillingItemModel sm)
        {
              int result = 0;
            using (var ct = new billingEntities())
            {
                ct.purchasebillingitems.Add(new purchasebillingitem()
                {
                   ItemId=sm.ItemId,
                   PurchaseId=sm.PurchaseId,
                   ItemTax=sm.ItemTax,
                   ItemPrice=sm.ItemPrice,
                   ItemQuantity=sm.ItemQuantity,
                   NetAmount=sm.NetAmount,
                   TaxAmount=sm.TaxAmount,
                   Type=1,
                   CreatedOn=DateTime.Now,
                   UpdatedOn=DateTime.Now,
                });
                 result = ct.SaveChanges();
            }
            if (result > 0)
            {
                return Json("{resultCode:1,message:sucess}");
            }
            else
            {
                return Json("{resultCode:0,message:faill}");
            }
        }
    }
}
