﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using API.Models;
using System.Data.Entity;

namespace API.Controllers
{
    public class DistributorController : ApiController
    {
        private billingEntities DB = new billingEntities();

        [HttpGet]
        public IHttpActionResult GetAllDistributors()
        {
            var result = from Distributors in DB.customerdetails
                         select new
                         {
                             Distributors.CustomerId,
                             Distributors.Address,
                             Distributors.LocationId,
                             Distributors.EmailID,
                             Distributors.PhoneNumber,
                             Distributors.CreatedOn,
                             Distributors.UpdatedOn,
                         };
            return Ok(result);
        }
        [HttpGet]
        public IHttpActionResult Distributors(int Id)
        {
            var result = from Distributor in DB.customerdetails
                         where Distributor.CustomerId.Equals(Id)
                         select new
                         {
                             Distributor.CustomerId,
                             Distributor.Address,
                             Distributor.LocationId,
                             Distributor.EmailID,
                             Distributor.PhoneNumber,
                             Distributor.CreatedOn,
                             Distributor.UpdatedOn,
                         };
            return Json(result);
        }
        [HttpPost]
        public IHttpActionResult InsertDistributorDetails(DistributorModel dm)
        {
            int result = 0;
            using (var ctx = new billingEntities())
            {
                ctx.customerdetails.Add(new customerdetail()
                {
                    CustomerName = dm.CustomerName,
                    Address = dm.Address,
                    LocationId=dm.LocationId,
                    EmailID=dm.EmailID,
                    PhoneNumber=dm.PhoneNumber,
                    CreatedOn=DateTime.Now,
                    UpdatedOn=DateTime.Now,
                });
                
                result = ctx.SaveChanges();
            }
            if (result > 0)
            {
                return Json("{resultCode:1,message:sucess}");
            }
            else
            {
                return Json("{resultCode:0,message:faill}");

            }
        }
        public IHttpActionResult GetDistributorByName(string name)
       {
            return Json("json");
        }
    }
}