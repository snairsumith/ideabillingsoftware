﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using API.Models;
using System.Data.Entity;



namespace API.Controllers
{
    public class SupplierController : ApiController
    {
        private billingEntities DB =new billingEntities();


        [HttpGet]
        public IHttpActionResult GetAllSuppliers()                                          
        {

            var result = from supplier in DB.supplierdetails
                         select new
                         {
                             supplier.SupplierId,
                             supplier.Address,
                             supplier.EmailID,
                             supplier.PhoneNumber,
                             supplier.LocationId,
                             supplier.GstNo,
                             supplier.CreatedOn,
                             supplier.UpdatedOn,

                         };
            return Ok(result);
        }
        [HttpGet]
        public IHttpActionResult SuppilerById(int id)
        {

            var result = from suppliers in DB.supplierdetails
                         where suppliers.SupplierId.Equals(id)
                         select new
                         {
                             suppliers.SupplierId,
                             suppliers.Address,
                             suppliers.EmailID,
                             suppliers.PhoneNumber,
                             suppliers.LocationId,
                             suppliers.GstNo,
                             suppliers.CreatedOn,
                             suppliers.UpdatedOn,

                         };
            return Json(result);
        }
        [HttpPost]
        public IHttpActionResult InsertData(SupplierModel ss)
        {
            if (ss.LocationId == null)
            {
                return Json("location required");
            }
            int result = 0;
            using (var ctx = new billingEntities())
            {
                ctx.supplierdetails.Add(new supplierdetail()
                {
                    Address = ss.Address,
                    LocationId = ss.LocationId,
                    EmailID = ss.EmailID,
                    PhoneNumber = ss.PhoneNumber,
                    GstNo = ss.GstNo,
                    UpdatedOn = DateTime.Now,
                    CreatedOn = DateTime.Now,


                });

                result = ctx.SaveChanges();
            }
            if (result > 0)
            {
                return Json("{resultCode:1,message:sucess}");
            }
            else
            {
                return Json("{resultCode:0,message:faill}");

            }

        }
    }
}


