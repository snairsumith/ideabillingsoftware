﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using API.Models;
using System.Data.Entity;

namespace API.Controllers
{
    public class ItemController : ApiController
    {
        private billingEntities DB = new billingEntities();


        [HttpGet]
        public IHttpActionResult GetAllItems()
        {

            var result = from Item in DB.items
                         select new
                         {
                             Item.ItemId,
                             Item.HsnId,
                             Item.ItemName,
                             Item.ItemDescription,
                             Item.ItemQuantity,
                             Item.ItemPrice,
                             Item.CreatedOn,
                             Item.UpdatedOn,

                         };
            return Ok(result);
        }
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {

            var result = from Item in DB.items
                         where Item.ItemId.Equals(id)
                         select new
                         {
                             Item.ItemId,
                             Item.HsnId,
                             Item.ItemName,
                             Item.ItemDescription,
                             Item.ItemQuantity,
                             Item.ItemPrice,
                             Item.CreatedOn,
                             Item.UpdatedOn,

                         };
            return Json(result);
        }
        [HttpPost]
        public IHttpActionResult InsertItem(ItemModel im)
        {
            int result = 0;
            using(var ctx=new billingEntities())
            {
                ctx.items.Add(new item()
                {
                    HsnId=im.HsnId,
                    ItemName=im.ItemName,
                    ItemDescription=im.ItemDescription,
                    ItemPrice=im.ItemPrice,
                    ItemQuantity=im.ItemQuantity,
                    CreatedOn=DateTime.Now,
                    UpdatedOn=DateTime.Now,
                });
                 result = ctx.SaveChanges();
            }
            if (result > 0)
            {
                return Json("{resultCode:1,message:sucess}");
            }
            else
            {
                return Json("{resultCode:0,message:faill}");

            }
         }
      }

}

