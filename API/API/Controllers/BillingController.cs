﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class BillingController : ApiController
    {
        private billingEntities DB = new billingEntities();
        [HttpGet]
        public IHttpActionResult GetAllBills()
        {
            var result = from bills in DB.billings
                         select new
                         {
                             bills.InvoiceId,
                             bills.CustomerId,
                             bills.BillNo,
                             bills.BillDate,
                             bills.PaymentStatus,
                             bills.TotalAmount,
                             bills.CreatedOn,
                             bills.UpdatedOn,
                         };
            return Ok(result);

        }
        [HttpGet]
        public IHttpActionResult GetBillingDetailsOfPurchasedItem(int id)
        {
            var result = from bills in DB.purchasebillingitems
                         where bills.ItemId.Equals(id) 
                         select new
                         {
                             bills.ItemId,
                             bills.PurchaseId,
                             bills.ItemTax,
                             bills.ItemPrice,
                             bills.ItemQuantity,
                             bills.NetAmount,
                             bills.TaxAmount,
                             bills.CreatedOn,
                             bills.UpdatedOn,
                            
                         };
            return Json(result);
        }

        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            var result = from log in DB.billings
                         where log.InvoiceId.Equals(id)
                         select new
                         {

                             log.InvoiceId,
                             log.CustomerId,
                             log.BillNo,
                             log.BillDate,
                             log.PaymentStatus,
                             log.TotalAmount,
                             log.CreatedOn,
                             log.UpdatedOn,

                         };
            return Json(result);
        }
        [HttpPost]
        public IHttpActionResult InsertBillingDetails(BillingModel bm)
        {
            int result = 0;
            using (var ctx = new billingEntities())
            {
                ctx.billings.Add(new billing()
                {
                    CustomerId=bm.CustomerId,
                    BillNo=bm.BillNo,
                    BillDate=DateTime.Now,
                    PaymentStatus = bm.PaymentStatus,
                    TotalAmount = bm.TotalAmount,
                    UpdatedOn = DateTime.Now,
                    CreatedOn = DateTime.Now,


                });

                result = ctx.SaveChanges();
            }
            if (result > 0)
            {
                return Json("{resultCode:1,message:sucess}");
            }
            else
            {
                return Json("{resultCode:0,message:faill}");

            }

        }
       
    }
}
